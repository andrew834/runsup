# Author #

**Full name:** Andrej Martinovič

**Email:** am6694@student.uni-lj.si

# Project description #

RunsUp is an Android application designed to track certain activities for users. It is mainly focused on physical activities such as running, walking, hiking and so on.
Users can select different sports to measure and track. At the end of each workout they can also share their results via famous social media applications. The application
enables the user to check the route they were on when doing the workout via the Google Maps API.

# Used libraries #

com.google.android.gms.location;

com.github.AnyChart:AnyChart-Android:1.1.2

com.google.firebase:firebase-core:16.0.9

com.google.firebase:firebase-auth:17.0.0

com.google.firebase:firebase-firestore:19.0.0

com.j256.ormlite:ormlite-core:5.0

com.j256.ormlite:ormlite-android:5.0

...

# Instructions for Sprint 4 #

RunsUp in Sprint 4 includes three new features. It allows the user to have a todo list, which is accessible from the main navigation menu. A user can preview, add, update and delete their todos.
If the user is logged in he can synchronize his workouts and todo list (a user can refresh the list view by swiping down). Another functionality is that the user has a Countdown timer, which is
accessible from the main navigation menu. Default time for the countdown is 5 minutes (can be changed). After pressing start, the user can refresh the timer by presing the refresh button or stop the
timer by pressing the pause button. After the timer reaches zero it sends the notification to the user and vibrates for about 1500ms (usefull if the user doesn't have the phone in his hand). The last
functionality is that the user can calculate his BMI (can be reached from the navigation menu). The BMI calculator contains a graph of average BMIs for USA, which can be used as a reference to the users
BMI. After filling all the fields, the user will be notified what his BMI is (besides that the graph will update the data based on the selected gender -> the default is male).

Main theme has been changed to a dark bluish color and the launcer icon contains an image that is appropriate for a fitness app. 