package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.model.TodoEntry;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class TodoFragment extends Fragment {

    @BindView(R.id.todo_entry_title)
    public EditText mTitle;

    @BindView(R.id.todo_entry_description)
    public EditText mDescription;

    @BindView(R.id.todo_time_picker)
    public Button mTimePicker;

    @BindView(R.id.todo_hour_picked)
    public TextView mPickedTextView;

    @BindView(R.id.todo_calender_pick)
    public CalendarView mCalendarView;

    @BindView(R.id.todo_save)
    public Button mSaveTodo;


    public static TodoFragment newInstance() {
        TodoFragment fragment = new TodoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private String mCurrentDate = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_todo_entry, container, false);
        ButterKnife.bind(this, mainView);

        return mainView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        mCalendarView.setOnDateChangeListener( new CalendarView.OnDateChangeListener() {
            public void onSelectedDayChange(CalendarView calendarView, int year, int month, int dayOfMonth) {
                String m = ""+(month+1);
                String d = ""+dayOfMonth;
                if((month+1) < 9){
                    m = "0"+(month+1);
                }

                if(dayOfMonth <= 9){
                    d = "0"+dayOfMonth;
                }
                mCurrentDate = (year + "-" + m + "-" + d);
            }
        });
    }

    @OnClick(R.id.todo_time_picker)
    public void openTimePicker(View v){
        Log.d("TodoFragment", "openTimePicker");

        DialogFragment newFragment = new TimePickerFragment();
        newFragment.setTargetFragment(this, 9002);
        newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 9002){
            int hours = data.getIntExtra("hours",12);
            int minutes = data.getIntExtra("minutes",0);
            minutes += hours*60;

            String displayTime = "Selected: " + hmsTimeFormatter(minutes*60*1000);

            mPickedTextView.setText(displayTime);
        }
    }

    @OnClick(R.id.todo_save)
    public void saveTodo(){
        String todoTitle = mTitle.getText().toString();
        String todoDescription = mDescription.getText().toString();
        String todoTime = mPickedTextView.getText().toString().split("Selected: ")[1];

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String selectedDate = "";

        if(mCurrentDate.equals("")){
            selectedDate = sdf.format(new Date());
            Log.d("123123", selectedDate);
        }else{
            Log.d("123123",mCurrentDate);
            selectedDate = mCurrentDate;
        }

        Log.d("convert", ""+new Date(mCalendarView.getDate()));

        if(todoTitle == null || todoTitle.length() == 0 || todoDescription == null || todoDescription.length() == 0 || todoTime == null || selectedDate == null){
            Toast.makeText(getActivity(), "Please fill all the fields", Toast.LENGTH_SHORT).show();
        }else{
            insertTodoIntoDatabase(todoTitle, todoDescription, selectedDate, todoTime);
            getActivity().onBackPressed();
        }
    }

    private void insertTodoIntoDatabase(String title, String description, String date, String time){

        Log.d("convert", date + " " + time);

        LocalDate datePart = LocalDate.parse(date);
        LocalTime timePart = LocalTime.parse(time);
        LocalDateTime dt = LocalDateTime.of(datePart, timePart);
        Date mDate = java.util.Date
                     .from(dt.atZone(ZoneId.systemDefault())
                        .toInstant());

        getDaos();
        User me = returnUser();

        TodoEntry myEntry = new TodoEntry(title, description, mDate, me);
        try {
            todoDao.create(myEntry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private User returnUser(){
        try {
            List<User> me = userDao.queryForAll();
            return me.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String hmsTimeFormatter(long milis) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(milis),
                TimeUnit.MILLISECONDS.toMinutes(milis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milis)),
                TimeUnit.MILLISECONDS.toSeconds(milis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milis)));
    }

    private Dao<User, Long> userDao;
    private Dao<TodoEntry, Long> todoDao;

    private void getDaos(){
        userDao = null;
        todoDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        try{
            userDao = databaseHelper.userDao();
            todoDao = databaseHelper.todoEntryDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
}
