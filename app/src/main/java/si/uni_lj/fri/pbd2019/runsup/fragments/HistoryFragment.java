package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import si.uni_lj.fri.pbd2019.runsup.Commands;
import si.uni_lj.fri.pbd2019.runsup.HistoryAdapter;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SyncWithServer;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.WorkoutSync;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class HistoryFragment extends Fragment {

    private SwipeRefreshLayout mSwipeRefreshLayout;

    public HistoryFragment() {}

    HistoryAdapter historyAdapter;
    private User mUser;
    ListView listView;
    ArrayList<String> strings;

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        getDaos();
        strings = new ArrayList<String>();

        try {
            mUser = userDao.queryForAll().get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Dao<Workout, Long> workoutsDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private Dao<User, Long> userDao;

    private void getDaos(){
        workoutsDao = null;
        gpsPointsDao = null;
        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        try{
            workoutsDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
            userDao = databaseHelper.userDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void getDataFromAPI(ListView listView, View parent) {
        //ForeignCollection<Workout> workouts = mUser.getWorkouts();
        List<Workout> workouts = null;
        try {
            workouts = (List<Workout>)workoutsDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("getDataFromAPI", ""+e.toString());
        }

        if(workouts == null) {
            return;
        }

        strings.clear();

        for(Workout iterate : workouts){
            if(iterate.getStatus() == Workout.statusDeleted || iterate.getStatus() == Workout.statusPaused || iterate.getStatus() == Workout.statusUnknown){
                continue;
            }
            String info = "";
            info += iterate.getTitle()+"&#&";
            info += iterate.getCreated()+"&#&";

            String datum = MainHelper.formatDuration(iterate.getDuration());

            if(datum.charAt(0) == '0'){
                datum = datum.substring(1);
            }

            String distanceD;
            String paceP;

            if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("unit", "0").equals("0")){
                // default enota
                String paceUnit = getResources().getString(R.string.all_min) + "/" + getResources().getString(R.string.all_labeldistanceunitkilometers);
                distanceD = MainHelper.formatDistance(iterate.getDistance()) + " " + getResources().getString(R.string.all_labeldistanceunitkilometers);
                paceP = MainHelper.formatPace(iterate.getPaceAvg()) + " " + paceUnit;
            }else{
                // miles
                String paceUnit = getResources().getString(R.string.all_min) + "/" + getResources().getString(R.string.all_labeldistanceunitmiles);
                distanceD = MainHelper.formatTwoDecimals(MainHelper.kmToMi(iterate.getDistance()/1000)) + " " + getResources().getString(R.string.all_labeldistanceunitmiles);
                paceP = MainHelper.formatPace(MainHelper.minpkmToMinpmi(iterate.getPaceAvg())) + " " + paceUnit;
            }

            info += datum + " ";
            info += Commands.ACTIVITY_TYPE_STRING.get(iterate.getSportActivity())+ " | ";
            info += distanceD + " | ";
            info += MainHelper.formatCalories(iterate.getTotalCalories()) + " kcal | avg ";
            info += paceP;

            info += "&#&"+iterate.getId();

            strings.add(info);
        }

        /*
        {{
            add("Workout 123&#&apr 35, 2017 11:36:38 PM&#&0:12:48 Running | 12.6 km | 1547 kcal | avg 12.45 min/km");
            add("Workout 123&#&apr 35, 2017 11:36:38 PM&#&0:12:48 Running | 12.6 km | 1547 kcal | avg 12.45 min/km");
            add("Workout 123&#&apr 35, 2017 11:36:38 PM&#&0:12:48 Running | 12.6 km | 1547 kcal | avg 12.45 min/km");
            add("Workout 123&#&apr 35, 2017 11:36:38 PM&#&0:12:48 Running | 12.6 km | 1547 kcal | avg 12.45 min/km");
            add("Workout 123&#&apr 35, 2017 11:36:38 PM&#&0:12:48 Running | 12.6 km | 1547 kcal | avg 12.45 min/km");
        }}
         */

        if(strings.size() > 0){
            ((TextView)parent.findViewById(R.id.textview_history_noHistoryData)).setVisibility(TextView.INVISIBLE);
        }else{
            ((TextView)parent.findViewById(R.id.textview_history_noHistoryData)).setVisibility(TextView.VISIBLE);
        }

        if(historyAdapter == null) {
            historyAdapter = new HistoryAdapter(getActivity(), 0, strings);
            listView.setAdapter(historyAdapter);
        }else{
            historyAdapter.clear();
            historyAdapter.notifyDataSetChanged();
            //historyAdapter.updateList(strings);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("TTT","onCreateView");
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        listView = view.findViewById(R.id.listview_history_workouts);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_history);
        final HistoryFragment myself = this;
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                FirebaseUser user = auth.getCurrentUser();
                if(user != null){
                    SyncWithServer sync = new SyncWithServer(getActivity(), true, listView, getView(), mSwipeRefreshLayout, myself);
                    sync.syncApp();
                }else{
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getDataFromAPI(listView, getView());
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.option_clear_history).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.option_clear_history:
                createDialog(getResources().getString(R.string.historysettings_actionclearhistory));
                break;
            case R.id.option_sync:
                SyncWithServer sync = new SyncWithServer(getActivity());
                sync.syncApp();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setupActionBar() {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void createDialog(String title){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        mBuilder.setTitle(title);

        mBuilder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    List<Workout> list = workoutsDao.queryForAll();
                    for(Workout w : list){
                        w.setStatus(Workout.statusDeleted);
                        workoutsDao.update(w);
                    }
                    getDataFromAPI(listView, getView());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dialogInterface.dismiss();
            }
        });

        mBuilder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        mBuilder.show();
    }
}
