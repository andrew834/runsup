package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mGoogleMap;
    private Workout mWorkout;
    private ArrayList<GpsPoint> workoutPoints;

    private Dao<Workout, Long> workoutsDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private Dao<User, Long> userDao;

    private void getDaos(){
        workoutsDao = null;
        gpsPointsDao = null;
        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try{
            workoutsDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
            userDao = databaseHelper.userDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_maps_workoutmap);
        mapFragment.getMapAsync(this);
        getDaos();

        try {
            mWorkout = ((List<Workout>)workoutsDao.queryForEq("id", getIntent()
                    .getLongExtra("workoutId", 1))).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        workoutPoints = new ArrayList<>();

        for (GpsPoint iterate : mWorkout.getGpsPoints()){
            workoutPoints.add(iterate);
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                ArrayList<LatLng> points = new ArrayList<>();
                LatLngBounds.Builder locationMap = new LatLngBounds.Builder();

                for (GpsPoint iterate : workoutPoints){
                    points.add(new LatLng(iterate.getLatitude(), iterate.getLongitude()));
                    locationMap.include(new LatLng(iterate.getLatitude(), iterate.getLongitude()));
                }

                mGoogleMap.addMarker(new MarkerOptions().position(points.get(0)).title("Start"));
                mGoogleMap.addMarker(new MarkerOptions().position(points.get(points.size()-1)).title("Finish"));

                for (int i = 0; i < points.size() - 1; i++){
                    if(workoutPoints.get(i).getSessionNumber() == workoutPoints.get(i+1).getSessionNumber()){
                        mGoogleMap.addPolyline(new PolylineOptions()
                                .add(points.get(i), points.get(i+1))
                                .width(5)
                                .color(Color.BLUE));
                    }else{
                        double dist = SphericalUtil.computeDistanceBetween(points.get(i), points.get(i+1));
                        if(dist < 100){
                            mGoogleMap.addMarker(new MarkerOptions().position(points.get(i))
                                    .title("Break " + workoutPoints.get(i).getSessionNumber()));
                            mGoogleMap.addPolyline(new PolylineOptions()
                                    .add(points.get(i), points.get(i+1))
                                    .width(5)
                                    .color(Color.BLUE));
                        }else{
                            mGoogleMap.addMarker(new MarkerOptions().position(points.get(i))
                                    .title("Pause " + workoutPoints.get(i).getSessionNumber()));
                            mGoogleMap.addMarker(new MarkerOptions().position(points.get(i))
                                    .title("Continue " + workoutPoints.get(i+1).getSessionNumber()));
                        }
                    }
                }

                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(locationMap.build(), 50));
            }
        });
    }
}
