package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.TodoReminderFragment;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.TodoEntry;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class SyncWithServer {

    private Context context;
    private FirebaseAuth mFirebaseAuth;
    private boolean refresh;
    private boolean todoRefresh;
    private ListView mListView;
    private View mView;
    private SwipeRefreshLayout mSRL;
    private HistoryFragment mHistoryFragment;
    private TodoReminderFragment mTodoReminderFragment;

    public SyncWithServer(Context context){
        this.context = context;
        this.mFirebaseAuth = FirebaseAuth.getInstance();
        this.refresh = false;
    }

    public SyncWithServer(Context context, boolean refresh, ListView mListView, View mView, SwipeRefreshLayout mSRL, HistoryFragment mHistoryFragment){
        this.context = context;
        this.mFirebaseAuth = FirebaseAuth.getInstance();
        this.refresh = refresh;
        this.mListView = mListView;
        this.mView = mView;
        this.mSRL = mSRL;
        this.mHistoryFragment = mHistoryFragment;
    }

    public SyncWithServer(Context context, boolean todoRefresh, ListView mListView, View mView, SwipeRefreshLayout mSRL, TodoReminderFragment mTodoReminderFragment){
        this.context = context;
        this.mFirebaseAuth = FirebaseAuth.getInstance();
        this.todoRefresh = todoRefresh;
        this.mListView = mListView;
        this.mView = mView;
        this.mSRL = mSRL;
        this.mTodoReminderFragment = mTodoReminderFragment;
    }

    private Dao<Workout, Long> workoutsDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private Dao<TodoEntry, Long> todoEntryDao;

    private void getDaos(){
        workoutsDao = null;
        gpsPointsDao = null;
        todoEntryDao = null;

        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);

        try{
            workoutsDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
            todoEntryDao = databaseHelper.todoEntryDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void syncApp(){
        FirebaseUser user = mFirebaseAuth.getCurrentUser();
        getDaos();
        if(user == null)
            return;

        try {
            final List<Workout> localWorkouts = workoutsDao.queryForAll();
            final List<TodoEntry> localTodoEntrys = todoEntryDao.queryForAll();
            getFromFirebase(localWorkouts, user.getUid());
            getFromFirebaseTodo(localTodoEntrys, user.getUid());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void configFirebase(final String uid){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").document(uid)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        updateFirebase(uid);
                        Log.d("deleteFirebase", "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("deleteFirebase", "Error deleting document", e);
                    }
                });

    }

    public void updateFirebase(String uid){
        List<Workout> workouts = null;

        try {
            workouts = workoutsDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(workouts == null || workouts.size() == 0){
            return;
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String,Object> myWorkoutData = new HashMap<>();
        int i = 0;
        for(Workout iterate : workouts){
            if(iterate == null || iterate.getStatus() == Workout.statusPaused || iterate.getStatus() == Workout.statusUnknown)
                continue;

            Map<String, Object> w = new HashMap<>();
            w.put("title", iterate.getTitle());
            w.put("created", iterate.getCreated());
            w.put("status", iterate.getStatus());
            w.put("distance", iterate.getDistance());
            w.put("duration", iterate.getDuration());
            w.put("totalCalories", iterate.getTotalCalories());
            w.put("paceAvg", iterate.getPaceAvg());
            w.put("sportActivity", iterate.getSportActivity());
            w.put("lastUpdate", iterate.getLastUpdate());

            ArrayList<Map<String,Object>> temp = new ArrayList<>();

            for(GpsPoint iterateGps : iterate.getGpsPoints()){
                if(iterateGps == null)
                    continue;

                Map<String, Object> gpsPointsMap = new HashMap<>();
                gpsPointsMap.put("sessionNumber", iterateGps.getSessionNumber());
                gpsPointsMap.put("latitude", iterateGps.getLatitude());
                gpsPointsMap.put("longitude", iterateGps.getLongitude());
                gpsPointsMap.put("duration", iterateGps.getDuration());
                gpsPointsMap.put("speed", iterateGps.getSpeed());
                gpsPointsMap.put("pace", iterateGps.getPace());
                gpsPointsMap.put("totalCalories", iterateGps.getTotalCalories());
                gpsPointsMap.put("created", iterateGps.getCreated());
                gpsPointsMap.put("lastUpdate", iterateGps.getLastUpdate());

                temp.add(gpsPointsMap);
            }

            w.put("gpsPoints", temp);
            myWorkoutData.put(""+i,w);
            i++;
        }

        db.collection("users").document(uid)
                .set(myWorkoutData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("FIREBASE123123", "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FIREBASE123123", "Error writing document", e);
                    }
                });

        if(refresh){
            mHistoryFragment.getDataFromAPI(mListView,mView);
            mSRL.setRefreshing(false);
        }

    }

    public void getFromFirebase(final List<Workout> localWorkouts, final String uid){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("users").document(uid);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {

                        Map<String, Object> data = document.getData(); // workouts in todoEntry

                        Log.d("getFirebaseData", "DocumentSnapshot data: " + document.getData());

                        if(data.size() == 0){
                            Log.d("getFirebaseData", "There was no saved workouts");
                            return;
                        }

                        for(String key : data.keySet()){ // CLOUD OBJECTS
                            HashMap<String, Object> workoutData = (HashMap<String, Object>) data.get(key);

                            boolean addToLocal = true;

                            for(Workout iterate : localWorkouts){
                                if(iterate == null || iterate.getStatus() == Workout.statusUnknown || iterate.getStatus() == Workout.statusPaused)
                                    continue;

                                Log.d("compareBoth", "iterate: "+iterate.getCreated()+ " workoutData: " + ((Timestamp)workoutData.get("created")).toDate());

                                if(iterate.getCreated().equals(((Timestamp)workoutData.get("created")).toDate())){
                                    Log.d("compareBoth", "we equal");
                                    addToLocal = false;
                                    break;
                                }
                            }

                            // dodat v lokalno ce velja boolean
                            if(addToLocal)
                                addToLocalBase(workoutData);

                            Log.d("getFirebaseData", ""+workoutData.get("created"));
                        }
                        configFirebase(uid);
                    } else {
                        Log.d("getFirebaseData", "No such document getFromFirebase");
                        updateFirebase(uid);
                    }
                } else {
                    Log.d("getFirebaseData", "get failed with ", task.getException());
                }
            }
        });
    }

    public void addToLocalBase(HashMap<String, Object> cloud){
        Workout newWorkout = new Workout();
        int statusXD = Math.toIntExact((long)cloud.get("status"));
        newWorkout.setStatus(statusXD);
        newWorkout.setTitle((String)cloud.get("title"));
        newWorkout.setCreated(((Timestamp)cloud.get("created")).toDate());
        newWorkout.setDistance((double)cloud.get("distance"));
        newWorkout.setDuration((long)cloud.get("duration"));
        newWorkout.setTotalCalories((double)cloud.get("totalCalories"));
        newWorkout.setPaceAvg((double)cloud.get("paceAvg"));
        int sportActivityXD = Math.toIntExact((long)cloud.get("sportActivity"));
        newWorkout.setSportActivity(sportActivityXD);
        newWorkout.setLastUpdate(((Timestamp)cloud.get("lastUpdate")).toDate());

        ArrayList<Map<String,Object>> meinPoints =  (ArrayList<Map<String,Object>>) cloud.get("gpsPoints");

        for(Map<String,Object> keyGps : meinPoints){

            HashMap<String, Object> hm = (HashMap<String, Object>)keyGps;

            GpsPoint newGpsPoint = new GpsPoint();
            newGpsPoint.setSessionNumber((long)hm.get("sessionNumber"));
            newGpsPoint.setLatitude((double)hm.get("latitude"));
            newGpsPoint.setLongitude((double)hm.get("longitude"));
            newGpsPoint.setDuration((long)hm.get("duration"));

            float speedXD = ((Double)hm.get("speed")).floatValue();

            newGpsPoint.setSpeed(speedXD);
            newGpsPoint.setPace((double)hm.get("pace"));
            newGpsPoint.setTotalCalories((double)hm.get("totalCalories"));
            newGpsPoint.setCreated(((Timestamp)hm.get("created")).toDate());
            newGpsPoint.setLastUpdate(((Timestamp)hm.get("lastUpdate")).toDate());

            newGpsPoint.setWorkout(newWorkout);

            try{
                gpsPointsDao.create(newGpsPoint);
            }catch(Exception e){
                // do nothing
            }
        }

        try {
            workoutsDao.create(newWorkout);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /* TODO lista */
    public void updateFirebaseTodoEntry(String uid){
        List<TodoEntry> todoEntries = null;

        try {
            todoEntries = todoEntryDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(todoEntries == null || todoEntries.size() == 0){
            return;
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String,Object> myTodoData = new HashMap<>();
        int i = 0;
        for(TodoEntry iterate : todoEntries){
            Map<String, Object> t = new HashMap<>();
            t.put("title", iterate.getTitle());
            t.put("whenDate", iterate.getWhenDate());
            t.put("description", iterate.getDescription());

            myTodoData.put(""+i,t);
            i++;
        }

        db.collection("todos").document(uid)
                .set(myTodoData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("FIREBASE123123", "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FIREBASE123123", "Error writing document", e);
                    }
                });

        if(todoRefresh){
            mTodoReminderFragment.getDataFromAPI(mListView,mView);
            mSRL.setRefreshing(false);
        }
    }

    public void addToLocalBaseTodo(HashMap<String, Object> cloud){
        TodoEntry newTodo = new TodoEntry();
        newTodo.setTitle((String)cloud.get("title"));
        newTodo.setWhenDate(((Timestamp)cloud.get("whenDate")).toDate());
        newTodo.setDescription((String)cloud.get("description"));

        try {
            todoEntryDao.create(newTodo);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getFromFirebaseTodo(final List<TodoEntry> localTodos, final String uid){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("todos").document(uid);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {

                        Map<String, Object> data = document.getData();

                        Log.d("getFirebaseData", "DocumentSnapshot data: " + document.getData());

                        if(data.size() == 0){
                            Log.d("getFirebaseData", "There was no saved todos");
                            return;
                        }

                        for(String key : data.keySet()){ // CLOUD OBJECTS
                            HashMap<String, Object> todoData = (HashMap<String, Object>) data.get(key);

                            boolean addToLocalTodo = true;

                            for(TodoEntry iterate : localTodos){
                                if(iterate.getWhenDate().equals(((Timestamp)todoData.get("whenDate")).toDate())){
                                    addToLocalTodo = false;
                                    break;
                                }
                            }

                            // dodat v lokalno ce velja boolean
                            if(addToLocalTodo)
                                addToLocalBaseTodo(todoData);

                            Log.d("getFirebaseData", ""+todoData.get("whenDate"));
                        }
                        configFirebaseTodo(uid);
                    } else {
                        Log.d("getFirebaseData", "No such document getFromFirebase");
                        updateFirebaseTodoEntry(uid);
                    }
                } else {
                    Log.d("getFirebaseData", "get failed with ", task.getException());
                }
            }
        });
    }

    public void configFirebaseTodo(final String uid){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("todos").document(uid)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        updateFirebaseTodoEntry(uid);
                        Log.d("deleteFirebase", "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("deleteFirebase", "Error deleting document", e);
                    }
                });
    }
}
