package si.uni_lj.fri.pbd2019.runsup.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.Commands;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class TrackerService extends Service{

    private final IBinder mBinder = new LocalBinder();
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Handler mHandler;

    private long duration = 0;
    private double distance = 0.0;
    private double pace = 0.0;
    private double calories = 0.0;
    private int CURRENT_STATE = 0;

    private Location mLastLocation;
    private ArrayList<Location> mCurrentLocations;
    private boolean mCheck100 = false;
    boolean mLastLocationSent = true;

    private ArrayList<Float> mSpeedList;
    private long mTimeBetweenPoints = 0;
    private int mActivityType = 0;
    private int DEFAULT_WEIGHT = 60;

    /* Baza attrs */
    private long databaseUpdateTimer = 0;
    private Workout mWorkout;
    private long sessionNumber = 1;
    private  Long workoutId = 1l;

    public int getState() {
        return 0;
    }

    public long getDuration() {
        return 0;
    }

    public double getDistance() {
        return 0.0;
    }

    public double getPace() {
        return 0.0;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public TrackerService getService() {
            return TrackerService.this;
        }
    }

    @Override
    public void onCreate(){
        super.onCreate();
        getDaos();
        //showToastText("onCreate");
        mCurrentLocations = new ArrayList<>();
        mSpeedList = new ArrayList<>();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(TrackerService.this);
        requestLocation();

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult mLocationResult) {
                super.onLocationResult(mLocationResult);
                List<User> u = null;
                QueryBuilder getUser = userDao.queryBuilder();

                try {
                    u = (List<User>)(getUser.query());
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                User userData = u.get(0);
                UserProfile up = null;
                try {
                    List<UserProfile> userProfiles = userData.getUserProfiles().getDao().queryBuilder().query();
                    if(userProfiles.size() > 0){
                        up = userProfiles.get(userProfiles.size() - 1);
                        DEFAULT_WEIGHT = (int)up.getWeight();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                //showToastText("onLocationResult");

                // dobimo podatke o lokaciji
                Location mCurrentLocation = mLocationResult.getLastLocation();

                if(mLastLocation != null){
                    double mDistance = mLastLocation.distanceTo(mCurrentLocation);

                    if(mCheck100 && mDistance <= 100){
                        mLastLocationSent = false;
                        distance += mDistance;
                        mCurrentLocations.add(mCurrentLocation);
                        mLastLocation = mCurrentLocation;
                        mTimeBetweenPoints = duration;
                        mCheck100 = false;
                        return;
                    }else if(mCheck100 && mDistance > 100){
                        mLastLocationSent = false;
                        mCurrentLocations.add(mCurrentLocation);
                        mLastLocation = mCurrentLocation;
                        mTimeBetweenPoints = duration;
                        mCheck100 = false;
                        return;
                    }

                    if(mDistance >= 2){
                        mLastLocationSent = false;
                        float mSpeed = (float)mDistance;

                        if(duration - mTimeBetweenPoints != 0){
                            mSpeed = mSpeed / (float)(duration - mTimeBetweenPoints);
                        }

                        mSpeedList.add(mSpeed);
                        if(mSpeedList.size() >= 2){
                            calories = SportActivities.countCalories(mActivityType, DEFAULT_WEIGHT, mSpeedList, ((double)duration)/3600);
                        }
                        Log.e(Commands.ERROR, mSpeedList.toString());
                        mCurrentLocations.add(mCurrentLocation);
                        mLastLocation = mCurrentLocation;
                        mTimeBetweenPoints = duration;
                        distance += mDistance;
                    }
                }else{
                    mLastLocationSent = false;
                    mCurrentLocations.add(mCurrentLocation);
                    mLastLocation = mCurrentLocation;
                    mTimeBetweenPoints = duration;
                }

                //showToastText("Location Update");
            }
        };
        mHandler = new Handler();
    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        try {
            mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        }catch(Exception e){
            Log.e(Commands.ERROR, e.toString());
        }

        //showToastText("onStartCommand");

        switch(intent.getAction()){
            case Commands.COMMAND_START:
                //showToastText("COMMAND_START");
                CURRENT_STATE = Commands.CURRENT_STATE.get("STATE_RUNNING");
                mHandler.post(task);
                getDaos();

                try {
                    Workout newWorkout = new Workout("Workout "+(workoutsDao.countOf()+1),intent.getIntExtra("sports_activity", 0));
                    mWorkout = newWorkout;
                    mWorkout.setCreated(new Date());
                    mWorkout.setStatus(Workout.statusUnknown);
                    User temp = userDao.queryForAll().get(0);
                    mWorkout.setUser(temp);
                    workoutsDao.create(mWorkout);
                    workoutId = mWorkout.getId();
                } catch (SQLException e) {
                    Log.d("SQL", "WE HAVE A PROBLEM " +  e.toString());
                    e.printStackTrace();
                }

                break;
            case Commands.COMMAND_CONTINUE:
                mCheck100 = true;
                //showToastText("COMMAND_CONTINUE");
                CURRENT_STATE = Commands.CURRENT_STATE.get("STATE_CONTINUE");

                sendBroadcast(updateFields());

                mHandler.postDelayed(task, 1000);

                CURRENT_STATE = Commands.CURRENT_STATE.get("STATE_RUNNING");
                sessionNumber++;
                break;
            case Commands.COMMAND_PAUSE:
                //showToastText("COMMAND_PAUSE");
                CURRENT_STATE = Commands.CURRENT_STATE.get("STATE_PAUSED");
                mHandler.removeCallbacks(task);
                mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                duration--;

                if(mWorkout != null){
                    sendBroadcast(updateFields());
                }
                break;
            case Commands.COMMAND_STOP:
                //showToastText("COMMAND_STOP");
                CURRENT_STATE = Commands.CURRENT_STATE.get("STATE_STOPPED");
                mHandler.removeCallbacks(task);
                stopSelf();
                duration--;

                if(mWorkout != null){
                    sendBroadcast(updateFields());
                }

                break;
            case Commands.UPDATE_SPORT_ACTIVITY:
                mActivityType = intent.getIntExtra("sports_activity", 0);

                if(mWorkout != null){
                    mWorkout.setSportActivity(mActivityType);
                }
                break;
            case Commands.SYNC_ACTIVITY:
                getDaos();

                QueryBuilder qb = workoutsDao.queryBuilder();
                Where where = qb.where();
                try {
                    mWorkout = (Workout) where.eq("id", intent.getLongExtra("workoutId",1)).query().get(0);
                    workoutId = intent.getLongExtra("workoutId",1);
                    Log.d("HAD","TRASL "+workoutId);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                duration = mWorkout.getDuration();
                distance = mWorkout.getDistance();
                pace = mWorkout.getPaceAvg();
                calories = mWorkout.getTotalCalories();
                CURRENT_STATE = Commands.CURRENT_STATE.get("STATE_PAUSED");
                mActivityType = mWorkout.getSportActivity();
                mTimeBetweenPoints = duration;

                ForeignCollection<GpsPoint> allPoints = mWorkout.getGpsPoints();

                int current = 0;

                for(GpsPoint iterate : allPoints){
                    //Log.d("GPS", ""+iterate.getId());
                    if(allPoints.size()-1 == current){
                        sessionNumber = iterate.getSessionNumber();
                    }
                    mSpeedList.add(iterate.getSpeed());
                    current++;
                }

                break;
        }

        return START_STICKY;
    }

    private void showToastText(String text){
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        Log.e(Commands.ERROR, "I am called TOAST");
    }

    private Runnable task = new Runnable() {
        @Override
        public void run() {
            // posiljamo podatke v receiver
            sendBroadcast(updateFields());
            mHandler.postDelayed(task, Commands.TASK_DELAY);
        }
    };

    private Intent updateFields(){
        duration++;
        Intent updates = new Intent(Commands.TICK);
        updates.putExtra("duration", duration);
        updates.putExtra("distance", distance);
        updates.putExtra("calories", calories);
        updates.putExtra("workoutId",workoutId);
        pace = 0.0;

        if(duration - mTimeBetweenPoints >= Commands.INTERVAL * 2 / 1000){
            updates.putExtra("pace", 0.0);
        }else{
            double faktor = 1.0/0.06;
            if(!mSpeedList.isEmpty()){
                pace = mSpeedList.get(mSpeedList.size() - 1) * faktor;
                updates.putExtra("pace", pace);
            }else{
                updates.putExtra("pace", 0.0);
            }
        }

        switch (CURRENT_STATE){
            case 0:
                // unknown
                mWorkout.setStatus(Workout.statusUnknown);
                break;
            case 1:
                mWorkout.setStatus(Workout.statusEnded);
                break;
            case 2:
                // paused
                Log.d("PAS","I AM PAUSED");
                mWorkout.setStatus(Workout.statusPaused);
                break;
            case 3:
                // continue
                mWorkout.setStatus(Workout.statusUnknown);
                break;
        }

        if(!mLastLocationSent){
            mLastLocationSent = true;
            updates.putExtra("location", mLastLocation);
            databaseUpdateTimer = 0;
            updateWorkout();
            try {
                workoutsDao.update(mWorkout);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            // DODAJ GPS TOCKO

            float speedGPS = 0;
            if(!mSpeedList.isEmpty()){
                speedGPS = mSpeedList.get(mSpeedList.size()-1);
            }

            double paceAvg = 0.0;

            if(distance > 0){
                paceAvg = duration / distance;
            }

            mWorkout.setPaceAvg(paceAvg);
            GpsPoint newPoint = new GpsPoint(mWorkout, sessionNumber, mLastLocation, duration, speedGPS, paceAvg, calories);
            newPoint.setCreated(new Date());
            newPoint.setLastUpdate(new Date());

            try {
                gpsPointsDao.create(newPoint);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else{
            if(databaseUpdateTimer > 10 || CURRENT_STATE != 0){
                // posodobi bazo ker je minilo 10 sekund
                databaseUpdateTimer = 0;
                updateWorkout();
                try {
                    workoutsDao.update(mWorkout);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        updates.putExtra("state", CURRENT_STATE);
        updates.putExtra("sportActivity", mActivityType);

        databaseUpdateTimer++;

        return updates;
    }

    public void updateWorkout(){
        mWorkout.setDistance(distance);
        mWorkout.setDuration(duration);
        mWorkout.setLastUpdate(new Date());
        mWorkout.setTotalCalories(calories);
        //mWorkout.setPaceAvg(pace);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        try {
            mHandler.removeCallbacks(task);
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }catch(Exception e){
            Log.e(Commands.ERROR, "I am only a student, have mercy!");
        }
    }

    public void requestLocation(){
        mLocationRequest = new LocationRequest()
                .setInterval(Commands.INTERVAL)
                .setSmallestDisplacement(Commands.DISPLACEMENT)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private Dao<Workout, Long> workoutsDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private Dao<User, Long> userDao;

    private void getDaos(){
        workoutsDao = null;
        gpsPointsDao = null;
        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try{
            workoutsDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
            userDao = databaseHelper.userDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
}
