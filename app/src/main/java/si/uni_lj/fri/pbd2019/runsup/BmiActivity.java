package si.uni_lj.fri.pbd2019.runsup;

import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BmiActivity extends AppCompatActivity {

    private List<DataEntry> data;
    private boolean isMale = true;

    @BindView(R.id.bmi_age)
    public EditText mBmiAge;

    @BindView(R.id.bmi_height)
    public EditText mBmiHeight;

    @BindView(R.id.bmi_weight)
    public EditText mBmiWeight;

    @BindView(R.id.bmi_radioGrp)
    public RadioGroup mRadioGroup;

    @BindView(R.id.bmi_calculate)
    public Button mBmiCalculate;

    @BindView(R.id.bmi_result)
    public TextView mBmiResult;

    private Cartesian mCartesian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);
        ButterKnife.bind(this);

        setupActionBar();


        initMales();
        drawGraph();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return true;
    }

    private void initMales(){
        isMale = true;
        data = new ArrayList<>();
        data.add(new ValueDataEntry("20-29", 25.5));
        data.add(new ValueDataEntry("30-39", 27.5));
        data.add(new ValueDataEntry("40-49", 28.5));
        data.add(new ValueDataEntry("50-59", 28.3));
        data.add(new ValueDataEntry("60-69", 28.0));
        data.add(new ValueDataEntry("70-79", 27.8));
        data.add(new ValueDataEntry("80+", 26.3));
    }

    private void drawGraph(){
        AnyChartView anyChartView = findViewById(R.id.any_chart_view);
        mCartesian = AnyChart.column();
        Column column = mCartesian.column(data);

        column.tooltip()
                .titleFormat("{%X}")
                .position(Position.CENTER_BOTTOM)
                .anchor(Anchor.CENTER_BOTTOM)
                .offsetX(0d)
                .offsetY(5d);

        mCartesian.animation(true);
        if(isMale){
            mCartesian.title("50th BMI percentile for males in USA");
        }else{
            mCartesian.title("50th BMI percentile for females in USA");
        }

        mCartesian.yScale().minimum(0d);

        mCartesian.yAxis(0).labels().format("{%Value}{groupsSeparator: }");

        mCartesian.tooltip().positionMode(TooltipPositionMode.POINT);
        mCartesian.interactivity().hoverMode(HoverMode.BY_X);

        mCartesian.xAxis(0).title("Age");
        mCartesian.yAxis(0).title("BMI");

        mCartesian.autoRedraw(true);

        anyChartView.setChart(mCartesian);

    }

    final int delayMillis = 500;
    final Handler handler = new Handler();
    final Runnable runnable = new Runnable() {
        public void run() {
            Log.d("updating123", "updating values");
            if(isMale){
                initMales();
            }else{
                initFemales();
            }

            if(isMale){
                mCartesian.title("50th BMI percentile for males in USA");
            }else{
                mCartesian.title("50th BMI percentile for females in USA");
            }

            mCartesian.animation(true);
            mCartesian.autoRedraw(true);
            mCartesian.data(data);
        }
    };

    private void initFemales(){
        isMale = false;
        data = new ArrayList<>();
        data.add(new ValueDataEntry("20-29", 25.6));
        data.add(new ValueDataEntry("30-39", 27.6));
        data.add(new ValueDataEntry("40-49", 28.1));
        data.add(new ValueDataEntry("50-59", 28.6));
        data.add(new ValueDataEntry("60-69", 28.9));
        data.add(new ValueDataEntry("70-79", 28.3));
        data.add(new ValueDataEntry("80+", 26.1));
    }

    @OnClick(R.id.bmi_calculate)
    public void calculateBmi(){
        int selectedId = mRadioGroup.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        RadioButton mGender = (RadioButton) findViewById(selectedId);

        Toast.makeText(this, "Gender is: " + mGender.getText(), Toast.LENGTH_SHORT).show();

        if(mGender.getText().toString().equals("Male")){
            isMale = true;
        }else{
            initFemales();
        }

        handler.postDelayed(runnable, delayMillis);

        if(mBmiAge.getText().toString().length() == 0 ||
                mBmiHeight.getText().toString().length() == 0 ||
                mBmiWeight.getText().toString().length() == 0){
            Toast.makeText(this, "Please fill all the fields.", Toast.LENGTH_SHORT).show();
            return;
        }

        int age = Integer.parseInt(mBmiAge.getText().toString());
        int height = Integer.parseInt(mBmiHeight.getText().toString());
        double weight = Double.parseDouble(mBmiWeight.getText().toString());

        double bmi = weight/(Math.pow(((double)height)/100.0,2));

        if(bmi < 16){
            mBmiResult.setText("Severe Thinness: "+bmi);
        }else if(bmi >= 16 && bmi < 17){
            mBmiResult.setText("Moderate Thinness: "+bmi);
        }else if(bmi >= 17 && bmi < 18.5){
            mBmiResult.setText("Mild Thinness: "+bmi);
        }else if(bmi >= 18.5 && bmi < 25){
            mBmiResult.setText("Normal: "+bmi);
        }else if(bmi >= 25 && bmi < 30){
            mBmiResult.setText("Overweight: "+bmi);
        }else if(bmi >= 30 && bmi < 35){
            mBmiResult.setText("Obese Class 1: "+bmi);
        }else if(bmi >= 35 && bmi < 40){
            mBmiResult.setText("Obese Class 2: "+bmi);
        }else if(bmi >= 40){
            mBmiResult.setText("Obese Class 3: "+bmi);
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
