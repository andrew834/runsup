package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.Commands;
import si.uni_lj.fri.pbd2019.runsup.HistoryAdapter;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.TodoAdapter;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SyncWithServer;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.TodoEntry;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class TodoReminderFragment extends Fragment {

    @BindView(R.id.todo_add)
    public FloatingActionButton mTodoAdd;

    private User mUser;
    private TodoAdapter mTodoAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    ListView listView;
    ArrayList<String> strings;

    public static TodoReminderFragment newInstance() {
        TodoReminderFragment fragment = new TodoReminderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDaos();

        strings = new ArrayList<String>();

        try {
            mUser = userDao.queryForAll().get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.todo_reminder_fragment, container, false);
        ButterKnife.bind(this, mainView);

        return mainView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        listView = view.findViewById(R.id.todo_list);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_todo);
        final TodoReminderFragment myself = this;
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                FirebaseUser user = auth.getCurrentUser();
                if(user != null){
                    SyncWithServer sync = new SyncWithServer(getActivity(), true, listView, getView(), mSwipeRefreshLayout, myself);
                    sync.syncApp();
                }else{
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    @OnClick(R.id.todo_add)
    public void addNewTodo(){
        FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_fragment, TodoFragment.newInstance()).addToBackStack( "tag" ).commit();
    }

    private Dao<TodoEntry, Long> todoDao;
    private Dao<User, Long> userDao;

    private void getDaos(){
        todoDao = null;
        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        try{
            todoDao = databaseHelper.todoEntryDao();
            userDao = databaseHelper.userDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDataFromAPI(listView, getView());
    }

    public void getDataFromAPI(ListView listView, View parent) {
        List<TodoEntry> todos = null;
        try {
            todos = todoDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("getDataFromAPI", ""+e.toString());
        }

        if(todos == null) {
            return;
        }

        strings.clear();

        for(TodoEntry iterate : todos){
            String info = "";
            info += iterate.getTitle()+"&#&";
            info += iterate.getWhenDate()+"&#&";
            info += iterate.getDescription();
            info += "&#&"+iterate.getId();

            strings.add(info);
        }

        mTodoAdapter = new TodoAdapter(getActivity(), 0, strings);
        listView.setAdapter(mTodoAdapter);
    }
}
