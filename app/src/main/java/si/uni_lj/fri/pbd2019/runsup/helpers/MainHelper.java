package si.uni_lj.fri.pbd2019.runsup.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class MainHelper {

    /* constants */
    private static final float MpS_TO_MIpH = 2.23694f;
    private static final float KM_TO_MI = 0.62137119223734f;
    private static final float MINpKM_TO_MINpMI = 1.609344f;
    private static final String dateFormat = "HH:mm:ss";

    /**
     * return string of time in format HH:MM:SS
     * @param time - in seconds
     */
    public static String formatDuration(long time) {
        int ure = (int) (time / 3600);
        int minute = (int) ((time / 60) % 60);
        int sekunde = (int) (time % 60);

        return addPadding(ure) + ":" + addPadding(minute) + ":" + addPadding(sekunde);
        //return new SimpleDateFormat(dateFormat).format(new Date(time * 1000));
    }

    private static String addPadding(int t){
        if(t < 10)
            return "0" + t;
        return "" + t;
    }

    /**
     * convert m to km and round to 2 decimal places and return as string
     */
    public static String formatDistance(double n) {
        return String.format("%.02f",Math.round(n/10.0) / 100.0); // uposteva kilometre
    }

    public static String formatTwoDecimals(double n) {
        return Double.toString( Math.round(n * 100.0) / 100.0); // uposteva kilometre
    }

    /**
     * round number to 2 decimal places and return as string
     */
    public static String formatPace(double n) {
        return String.format("%.02f",Math.round(n*100.0) / 100.0);
    }

    /**
     * round number to integer
     */
    public static String formatCalories(double n) {
        return Integer.toString((int)Math.round(n));
    }

    /* convert km to mi (multiply with a corresponding constant) */
    public static double kmToMi(double n) {
        return KM_TO_MI * n;
    }

    /* convert m/s to mi/h (multiply with a corresponding constant) */
    public static double mpsToMiph(double n) {
        return MpS_TO_MIpH * n;
    }

    /* convert min/km to min/mi (multiply with a corresponding constant) */
    public static double minpkmToMinpmi(double n) {
        return MINpKM_TO_MINpMI * n;
    }

    public static double kmphToMiph(double n) {
        return n / 1.609;
    }
}
