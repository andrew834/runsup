package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "UserProfile")
public class UserProfile {

    public UserProfile(){

    }

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(foreign = true, columnName = "user")
    private User user;

    @DatabaseField(columnName = "weight")
    private double weight;

    @DatabaseField(columnName = "height")
    private double height;

    @DatabaseField(columnName = "age")
    private Integer age;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
