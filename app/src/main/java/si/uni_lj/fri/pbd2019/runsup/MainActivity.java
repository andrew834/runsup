package si.uni_lj.fri.pbd2019.runsup;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import si.uni_lj.fri.pbd2019.runsup.fragments.AboutFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.CountdownFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.TodoFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.TodoReminderFragment;
import si.uni_lj.fri.pbd2019.runsup.helpers.SyncWithServer;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ImageView mImageView;
    private TextView mTextView;
    private Dao<User, Long> userDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mTextView = navigationView.getHeaderView(0).findViewById(R.id.menu_loggedInUserFullName);

        mImageView = navigationView.getHeaderView(0).findViewById(R.id.menu_loggedInUserImage);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });

        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);

        try{
            userDao = databaseHelper.userDao();
            QueryBuilder getUser = userDao.queryBuilder();
            List<User> u = (List<User>)(getUser.query());
            if(u == null || u.size() == 0){
                userDao.create(new User());
                Toast.makeText(this, "New user was created", Toast.LENGTH_SHORT).show();
             }
        }catch(SQLException e){
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = fragmentManager.beginTransaction();

        Fragment mFragment = null;

        mFragment = StopwatchFragment.newInstance();
        ft.replace(R.id.main_fragment, mFragment);

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();

     }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);

        boolean userLogedIn = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("userSignedIn",false);

        if(!userLogedIn){
            menu.getItem(1).setVisible(false);
        }else{
            menu.getItem(1).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.option_settings){
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }else if(id == R.id.option_sync){
            SyncWithServer sync = new SyncWithServer(this);
            sync.syncApp();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        FragmentManager fm = this.getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = fragmentManager.beginTransaction();

        Fragment mFragment = null;

        if(id == R.id.nav_workout){

            mFragment = StopwatchFragment.newInstance();
            ft.replace(R.id.main_fragment, mFragment);

            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();

        }else if(id == R.id.nav_history){

            mFragment = HistoryFragment.newInstance();
            ft.replace(R.id.main_fragment, mFragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            ft.commit();
        }else if(id == R.id.nav_settings){
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }else if(id == R.id.nav_about){
            mFragment = AboutFragment.newInstance();
            ft.replace(R.id.main_fragment, mFragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            ft.commit();
        }else if(id == R.id.nav_countdown){
            mFragment = CountdownFragment.newInstance();
            ft.replace(R.id.main_fragment, mFragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            ft.commit();
        }else if(id == R.id.nav_todo){
            mFragment = TodoReminderFragment.newInstance();
            ft.replace(R.id.main_fragment, mFragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            ft.commit();
        }else if(id == R.id.nav_bmi){
            Intent intent = new Intent(this, BmiActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        String fullname = PreferenceManager.getDefaultSharedPreferences(this).getString("fullname", getResources().getString(R.string.all_unknownuser));
        String picture = PreferenceManager.getDefaultSharedPreferences(this).getString("picture", "");

        mTextView.setText(fullname);

        if(picture.length() != 0){
            Glide.with(this).load(picture).into(mImageView);
        }else{
            mImageView.setImageResource(R.drawable.avatar_login);
        }

        try{
            boolean userLogedIn = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("userSignedIn",false);
            Toolbar toolbar = findViewById(R.id.toolbar);

            if(!userLogedIn){
                toolbar.getMenu().getItem(1).setVisible(false);
            }else{
                toolbar.getMenu().getItem(1).setVisible(true);
            }
        }catch(Exception e){
            Log.d("Deebugg", e.toString());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
