package si.uni_lj.fri.pbd2019.runsup;

import java.util.HashMap;
import java.util.TreeSet;

public final class Commands {
    public static final String COMMAND_START = "si.uni_lj.fri.pbd2019.runsup.COMMAND_START";
    public static final String COMMAND_CONTINUE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE";
    public static final String COMMAND_PAUSE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE";
    public static final String COMMAND_STOP = "si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP";
    public static final String TICK = "si.uni_lj.fri.pbd2019.runsup.TICK";
    public static final String UPDATE_SPORT_ACTIVITY = "si.uni_lj.fri.pbd2019.runsup.UPDATE_SPORT_ACTIVITY";
    public static final String SYNC_ACTIVITY = "si.uni_lj.fri.pbd2019.runsup.SYNC_ACTIVITY";
    public static final long INTERVAL = 3 * 1000;
    public static final long DISPLACEMENT = 3 * 1;
    public static final String ERROR = "ERROR";
    public static final long TASK_DELAY = 1000;

    public static final HashMap<String, Integer> CURRENT_STATE = new HashMap<String, Integer>(){{
        put("STATE_RUNNING", 0);
        put("STATE_STOPPED", 1);
        put("STATE_PAUSED", 2);
        put("STATE_CONTINUE", 3);
    }};

    public static final HashMap<String, Integer> ACTIVITY_TYPE = new HashMap<String, Integer>(){{
        put("RUNNING", 0);
        put("WALKING", 1);
        put("CYCLING", 2);
    }};

    public static final HashMap<Integer, String> ACTIVITY_TYPE_STRING = new HashMap<Integer, String>(){{
        put(0, "Running");
        put(1, "Walking");
        put(2, "Cycling");
    }};

    public static final String DIALOG_TAG = "DIALOG_TAG";
}
