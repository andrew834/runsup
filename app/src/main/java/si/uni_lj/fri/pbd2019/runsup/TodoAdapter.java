package si.uni_lj.fri.pbd2019.runsup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.fragments.TodoEditFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.TodoFragment;

public class TodoAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private ArrayList<String> mArrayList;
    private String[] elements;

    public TodoAdapter(@NonNull Context context, int resource, ArrayList<String> strings) {
        super(context, resource);
        this.mArrayList = strings;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String elements[] = mArrayList.get(position).split("&#&");
        Log.d("ADAP",elements.toString());
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        View v = layoutInflater.inflate(R.layout.adapter_todo, null);
        final Long id = Long.parseLong(elements[3]);

        String newDate = elements[1].split("GMT")[0];

        ((TextView)v.findViewById(R.id.textview_todo_title)).setText(elements[0]);
        ((TextView)v.findViewById(R.id.textview_short_description)).setText(elements[2]);
        ((TextView)v.findViewById(R.id.textview_todo_datetime)).setText(newDate);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction tx = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_fragment, TodoEditFragment.newInstance(id)).addToBackStack( "tag" ).commit();
            }
        });

        return v;
    }
}
