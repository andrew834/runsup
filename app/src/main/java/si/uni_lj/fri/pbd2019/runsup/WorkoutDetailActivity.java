package si.uni_lj.fri.pbd2019.runsup;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class WorkoutDetailActivity extends AppCompatActivity {

    private TextView mCal;
    private TextView mDur;
    private TextView mPac;
    private TextView mAct;
    private TextView mDis;
    private TextView mCurrentDate;

    double mCalories;
    long mDuration;
    double mPace;
    int mActivityType;
    double mDistance;
    long workoutId;

    private Workout currentWorkout;
    private GoogleMap mGoogleMap;

    ArrayList<GpsPoint> workoutPoints;

    @BindView(R.id.textview_workoutdetail_workouttitle)
    public TextView mWorkoutTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_detail);

        setupActionBar();

        ButterKnife.bind(this);

        getDaos();
        Intent data = getIntent();

        workoutId = data.getLongExtra("workoutId",1l);
        Date workoutDate = null;
        try {
            Workout workout = workoutsDao.queryForEq("id", workoutId).get(0);
            currentWorkout = workout;
            mCalories = workout.getTotalCalories();
            mDuration = workout.getDuration();
            mPace = workout.getPaceAvg();
            mActivityType = workout.getSportActivity();
            mDistance = workout.getDistance();
            workoutDate = workout.getCreated();
            // ZA PRIKAZ NA MAPI
            ForeignCollection<GpsPoint> points = workout.getGpsPoints();
            Log.d("TAGFG", ""+points.size());
            workoutPoints = new ArrayList<>();
            for (GpsPoint iterate : points){
                workoutPoints.add(iterate);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        /* SPRINT 2
        mCalories = data.getDoubleExtra("calories", 0.0);
        mDuration = data.getLongExtra("duration", 0);
        mPace = data.getDoubleExtra("pace", 0.0);
        mActivityType = data.getIntExtra("sportActivity", Commands.ACTIVITY_TYPE.get("RUNNING"));
        mDistance = data.getDoubleExtra("distance", 0.0);
        mLocations = (ArrayList<List<Location>>) data.getSerializableExtra("finalPositionList");
        */
        Log.d("HAD",""+workoutId);

        mCal = (TextView) findViewById(R.id.textview_workoutdetail_valuecalories);
        mDur = (TextView) findViewById(R.id.textview_workoutdetail_valueduration);
        mPac = (TextView) findViewById(R.id.textview_workoutdetail_valueavgpace);
        mAct = (TextView) findViewById(R.id.textview_workoutdetail_sportactivity);
        mDis = (TextView) findViewById(R.id.textview_workoutdetail_valuedistance);
        mCurrentDate = (TextView) findViewById(R.id.textview_workoutdetail_activitydate);

        ((TextView) findViewById(R.id.textview_workoutdetail_workouttitle)).setText(currentWorkout.getTitle());
        //paceUnit = getResources().getString(R.string.all_min) + "/" + getResources().getString(R.string.all_labeldistanceunitkilometers);
        mCal.setText(MainHelper.formatCalories(mCalories) + " " + getResources().getString(R.string.all_labelcaloriesunit));

        String dur = MainHelper.formatDuration(mDuration);


        if(dur.charAt(0) == '0'){
            dur = dur.substring(1);
        }

        mDur.setText(dur);

        if(PreferenceManager.getDefaultSharedPreferences(this).getString("unit", "0").equals("0")){
            // default enota
            String paceUnit = getResources().getString(R.string.all_min) + "/" + getResources().getString(R.string.all_labeldistanceunitkilometers);
            mDis.setText(MainHelper.formatDistance(mDistance) + " " + getResources().getString(R.string.all_labeldistanceunitkilometers));
            mPac.setText(MainHelper.formatPace(mPace) + " " + paceUnit);
        }else{
            // miles
            String paceUnit = getResources().getString(R.string.all_min) + "/" + getResources().getString(R.string.all_labeldistanceunitmiles);
            mDis.setText(MainHelper.formatTwoDecimals(MainHelper.kmToMi(mDistance/1000)) + " " + getResources().getString(R.string.all_labeldistanceunitmiles));
            mPac.setText(MainHelper.formatPace(MainHelper.minpkmToMinpmi(mPace)) + " " + paceUnit);
        }

        mCurrentDate.setText(SimpleDateFormat.getDateTimeInstance().format(workoutDate));
        mAct.setText(Commands.ACTIVITY_TYPE_STRING.get(mActivityType));

        if(workoutPoints.size() > 1) {
            findViewById(R.id.fragment_workoutdetail_map).setVisibility(View.VISIBLE);
            setSupportMapFragment();
        }else{
            findViewById(R.id.fragment_workoutdetail_map).setVisibility(View.GONE);
        }
    }

    public static class ShareMessageFragment extends DialogFragment {
        public static ShareMessageFragment newInstance(HashMap<String, String> args, boolean showImage) {
            ShareMessageFragment mSMF = new ShareMessageFragment();
            Bundle bundle = new Bundle();

            bundle.putString("type", args.get("type"));
            bundle.putString("distance", args.get("distance"));
            bundle.putString("unit", args.get("unit"));
            bundle.putString("duration", args.get("duration"));
            bundle.putBoolean("showImage", showImage);

            mSMF.setArguments(bundle);

            return mSMF;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.dialog_share_message, container);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            Bundle bundle = getArguments();

            ((EditText)(view.findViewById(R.id.share_message_textview))).setText("I was out for " +
                    bundle.getString("type") + ". I did " +
                    bundle.getString("distance") + " " +
                    bundle.getString("unit") + " in " +
                    bundle.getString("duration"));

            if(!bundle.getBoolean("showImage")){
                view.findViewById(R.id.image_map).setVisibility(View.GONE);
            }else{
                view.findViewById(R.id.image_map).setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            Fragment map_image = getFragmentManager().findFragmentById(R.id.image_map);
            if(map_image != null)
                getFragmentManager().beginTransaction().remove(map_image).commit();
        }
    }

    public void showWithMap(View v){
        createShareMessageFragment(true).show(getSupportFragmentManager(), Commands.DIALOG_TAG);}

    public void showWithoutMap(View v){
        createShareMessageFragment(false).show(getSupportFragmentManager(), Commands.DIALOG_TAG);
    }

    public ShareMessageFragment createShareMessageFragment(boolean do_this) {
        return ShareMessageFragment.newInstance(new HashMap<String, String>(){{
            put("type", Commands.ACTIVITY_TYPE_STRING.get(mActivityType));

            if(PreferenceManager.getDefaultSharedPreferences(WorkoutDetailActivity.this).getString("unit", "0").equals("0")){
                // default enota
                put("distance", MainHelper.formatDistance(mDistance));
                put("unit", getResources().getString(R.string.all_labeldistanceunitkilometers));
            }else{
                // miles
                put("distance", MainHelper.formatTwoDecimals(MainHelper.kmToMi(mDistance/1000)));
                put("unit", getResources().getString(R.string.all_labeldistanceunitmiles));
            }

            String datum = MainHelper.formatDuration(mDuration);

            if(datum.charAt(0) == '0'){
                datum = datum.substring(1);
            }

            put("duration", datum);
        }}, do_this);
    }

    private Dao<Workout, Long> workoutsDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private Dao<User, Long> userDao;

    private void getDaos(){
        workoutsDao = null;
        gpsPointsDao = null;
        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try{
            workoutsDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
            userDao = databaseHelper.userDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @OnClick(R.id.textview_workoutdetail_workouttitle)
    public void createDialog(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Update workout title");

        final EditText editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_TEXT);

        mBuilder.setView(editText);

        mBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String text = editText.getText().toString();
                currentWorkout.setTitle(text);
                mWorkoutTitle.setText(text);
                currentWorkout.setLastUpdate(new Date());
                try {
                    workoutsDao.update(currentWorkout);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        mBuilder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.option_delete:
                currentWorkout.setStatus(Workout.statusDeleted);
                try {
                    workoutsDao.update(currentWorkout);
                    this.finish();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.option_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.option_delete).setVisible(true);
        menu.findItem(R.id.option_sync).setVisible(false);

        return super.onPrepareOptionsMenu(menu);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void setSupportMapFragment(){
        SupportMapFragment smf = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_workoutdetail_map);
        smf.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;

                if(workoutPoints.size() <= 1){
                    return;
                }

                mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                        ArrayList<LatLng> points = new ArrayList<>();
                        LatLngBounds.Builder locationMap = new LatLngBounds.Builder();

                        for (GpsPoint iterate : workoutPoints){
                            points.add(new LatLng(iterate.getLatitude(), iterate.getLongitude()));
                            locationMap.include(new LatLng(iterate.getLatitude(), iterate.getLongitude()));
                        }

                        for (int i = 0; i < points.size() - 1; i++){
                            if(workoutPoints.get(i).getSessionNumber() == workoutPoints.get(i+1).getSessionNumber()){
                                mGoogleMap.addPolyline(new PolylineOptions()
                                        .add(points.get(i), points.get(i+1))
                                        .width(5)
                                        .color(Color.BLUE));
                            }else{
                                double dist = SphericalUtil.computeDistanceBetween(points.get(i), points.get(i+1));
                                if(dist < 100){
                                    //mGoogleMap.addMarker(new MarkerOptions().position(points.get(i))
                                           // .title("Break " + workoutPoints.get(i).getSessionNumber()));
                                    mGoogleMap.addPolyline(new PolylineOptions()
                                            .add(points.get(i), points.get(i+1))
                                            .width(5)
                                            .color(Color.BLUE));
                                }else{
                                   // mGoogleMap.addMarker(new MarkerOptions().position(points.get(i))
                                           // .title("Pause " + workoutPoints.get(i).getSessionNumber()));
                                   // mGoogleMap.addMarker(new MarkerOptions().position(points.get(i))
                                           // .title("Continue " + workoutPoints.get(i+1).getSessionNumber()));
                                }
                            }
                        }


                        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
                        mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);

                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(locationMap.build(), 50));

                        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                //Toast.makeText(WorkoutDetailActivity.this, "YES YES", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                                intent.putExtra("workoutId", currentWorkout.getId());
                                startActivity(intent);
                            }
                        });
                    }
                });
            }
        });
    }
}
