package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.util.Log;

import java.util.List;
import java.util.TreeMap;

public final class SportActivities {
    private static TreeMap<Integer, Double> runningMET = new TreeMap<Integer, Double>(){{
        put(4, 6.0);
        put(5, 8.3);
        put(6, 9.8);
        put(7, 11.0);
        put(8, 11.8);
        put(9, 12.8);
        put(10, 14.5);
        put(11, 16.0);
        put(12, 19.0);
        put(13, 19.8);
        put(14, 23.0);
    }};

    private static TreeMap<Integer, Double> walkingMET = new TreeMap<Integer, Double>(){{
        put(1, 2.0);
        put(2, 2.8);
        put(3, 3.1);
        put(4, 3.5);
    }};

    private static TreeMap<Integer, Double> cyclingMET = new TreeMap<Integer, Double>(){{
        put(10, 6.8);
        put(12, 8.0);
        put(14, 10.0);
        put(16, 12.8);
        put(18, 13.6);
        put(20, 15.8);
    }};

    private static final double runningAvgMET = 1.535353535;
    private static final double walkingAvgMET = 1.14;
    private static final double cyclingAvgMET = 0.744444444;

    public static final int RUNNING = 0;
    public static final int WALKING = 1;
    public static final int CYCLING = 2;

    /**
     * Returns MET value for an activity.
     *
     * @param activityType - sport activity type (0 - running, 1 - walking, 2 -
     *                     cycling)
     * @param speed        - speed in m/s
     * @return
     */
    public static double getMET(int activityType, Float speed) {
        double newSpeed = MainHelper.mpsToMiph(speed);
        int s = (int)Math.ceil(newSpeed);

        switch (activityType) {
            case 0:
                Double tR = runningMET.get(s);
                if(tR==null)
                    return newSpeed*runningAvgMET;
                return tR;
            case 1:
                Double tW = walkingMET.get(s);
                if(tW==null)
                    return newSpeed*walkingAvgMET;
                return tW;
            case 2:
                Double tC = cyclingMET.get(s);
                if(tC==null)
                    return newSpeed*cyclingAvgMET;
                return tC;
        }

        return 0.0;
    }

    /**
     * Returns final calories computed from the data provided (returns value in kcal)
     * @param sportActivity - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param weight - weight in kg
     * @param speedList - list of all speed values recorded (unit = m/s)
     * @param timeFillingSpeedListInHours - time of collecting speed list (duration of sport activity from first to last speedPoint in speedList)
     * @return
     */
    public static double countCalories(int sportActivity, float weight, List<Float> speedList, double timeFillingSpeedListInHours) {
        return getAvgMET(speedList, sportActivity) * weight * timeFillingSpeedListInHours;
    }

    public static double getAvgMET(List<Float> t, int activityType){
        double avgSpeed = 0.0;

        for (float i : t) {
            avgSpeed += i;
        }

        return getMET(activityType, (float)(avgSpeed / t.size()));
    }
}