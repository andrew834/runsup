package si.uni_lj.fri.pbd2019.runsup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;

public class HistoryAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private ArrayList<String> mArrayList;
    private String[] elements;

    public HistoryAdapter(@NonNull Context context, int resource, ArrayList<String> strings) {
        super(context, resource);
        mArrayList = strings;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String elements[] = mArrayList.get(position).split("&#&");
        Log.d("ADAP",elements.toString());
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        View v = layoutInflater.inflate(R.layout.adapter_history, null);
        final Long id = Long.parseLong(elements[3]);
        ((TextView)v.findViewById(R.id.textview_history_title)).setText(elements[0]);
        ((TextView)v.findViewById(R.id.textview_history_datetime)).setText(elements[1]);
        ((TextView)v.findViewById(R.id.textview_history_sportactivity)).setText(elements[2]);
        //((TextView)v.findViewById(R.id.textview_history_sportactivity)).setText(elements[3]);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("CLICKEDID", ""+id);
                Intent intent = new Intent(mContext, WorkoutDetailActivity.class);
                intent.putExtra("workoutId", id);
                mContext.startActivity(intent);
            }
        });

        return v;
    }
}
