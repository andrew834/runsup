package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import si.uni_lj.fri.pbd2019.runsup.R;

public class CountdownFragment extends Fragment {

    private long mCountdownMilis = 1 * 60000;

    private enum CountdownTimerEnum {
        RUNNING,
        FINISHED
    }

    private CountdownTimerEnum mCountdownTimer = CountdownTimerEnum.FINISHED;

    @BindView(R.id.progressBarCircle)
    public ProgressBar mProgressBarCircle;

    @BindView(R.id.inputMinutes)
    public EditText mInputMinutes;

    @BindView(R.id.textViewTime)
    public TextView mTextViewTime;

    @BindView(R.id.imageViewReset)
    public ImageView mImageViewReset;

    @BindView(R.id.imageViewStartStop)
    public ImageView mImageViewStartStop;

    private CountDownTimer mCountDownTimer;

    public static CountdownFragment newInstance() {
        CountdownFragment fragment = new CountdownFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_countdown, container, false);
        ButterKnife.bind(this, mainView);

        return mainView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        initAll();
    }

    private void initAll() {
        mImageViewReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.imageViewReset:
                        reset();
                        break;
                    case R.id.imageViewStartStop:
                        startStop();
                        break;
                }
            }
        });

        mImageViewStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.imageViewReset:
                        reset();
                        break;
                    case R.id.imageViewStartStop:
                        startStop();
                        break;
                }
            }
        });

        mInputMinutes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    TextView mTextViewTimer = getView().findViewById(R.id.textViewTime);

                    String requestedTime = ((EditText)v).getText().toString();
                    long requestedLength = Long.parseLong(requestedTime) * 60 * 1000;

                    mTextViewTimer.setText(hmsTimeFormatter(requestedLength));
                }
            }
        });
    }

    private void reset() {
        mCountDownTimer.cancel();
        startCountDownTimer();
    }

    private void startStop() {
        if (mCountdownTimer == CountdownTimerEnum.FINISHED) {
            setTimerValues();
            setProgressBarValues();
            reInit(false, View.VISIBLE, R.drawable.stop_countdown, CountdownTimerEnum.RUNNING);
            startCountDownTimer();
        } else {
            reInit(true, View.GONE, R.drawable.start_countdown, CountdownTimerEnum.FINISHED);
            mCountDownTimer.cancel();
        }
    }

    private void reInit(boolean flag, int visibility, int draw, CountdownTimerEnum state){
        mInputMinutes.setEnabled(flag);
        mImageViewReset.setVisibility(visibility);
        mImageViewStartStop.setImageResource(draw);
        mCountdownTimer = state;
    }

    private void setTimerValues() {
        if (!mInputMinutes.getText().toString().isEmpty()) {
            mCountdownMilis = 60 * 1000 * Integer.parseInt(mInputMinutes.getText().toString().trim());
        }
    }

    private void startCountDownTimer() {
        mCountDownTimer = new CountDownTimer(mCountdownMilis, 1000) {
            @Override
            public void onTick(long milisLeft) {
                mTextViewTime.setText(hmsTimeFormatter(milisLeft));
                mProgressBarCircle.setProgress((int) (milisLeft / 1000));
            }

            @Override
            public void onFinish() {
                mImageViewStartStop.setImageResource(R.drawable.start_countdown);
                mInputMinutes.setEnabled(true);
                mCountdownTimer = CountdownTimerEnum.FINISHED;
                mImageViewReset.setVisibility(View.GONE);

                if(getActivity() == null){
                    return;
                }

                Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(1500, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(1500);
                }

                sendNotification();
            }
        }.start();
        mCountDownTimer.start();
    }

    private void setProgressBarValues() {
        int progress = (int) mCountdownMilis / 1000;

        mProgressBarCircle.setMax(progress);
        mProgressBarCircle.setProgress(progress);
    }

    private String REQ_ID = "CountDown";

    public void sendNotification(){
        createNotificationChannel();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity(), REQ_ID)
                .setSmallIcon(R.drawable.ic_timer_black_24dp)
                .setContentTitle("RunsUp - Countdown has finished")
                .setContentText("Countdown has finished. Time: "+hmsTimeFormatter(mCountdownMilis))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Countdown has finished. Time: "+hmsTimeFormatter(mCountdownMilis)))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getActivity());
        notificationManager.notify(1, builder.build());
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = REQ_ID;
            String description = "Countdown timer";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(REQ_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getActivity().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    private String hmsTimeFormatter(long milis) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(milis),
                TimeUnit.MILLISECONDS.toMinutes(milis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milis)),
                TimeUnit.MILLISECONDS.toSeconds(milis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milis)));
    }
}
