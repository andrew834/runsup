package si.uni_lj.fri.pbd2019.runsup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class ActiveWorkoutMapActivity extends AppCompatActivity {

    private Handler mHandler;
    private Long mWorkoutId;
    private Long mTimer = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_workout_map);
        ButterKnife.bind(this);
        setSupportMapFragment();
        getDaos();
        mWorkoutId = getIntent().getLongExtra("workoutId", 0);
        try {
            ArrayList<Workout> w = (ArrayList<Workout>) (workoutsDao.queryForEq("id",mWorkoutId));

            if(w.size() != 0){
                mHandler = new Handler();
                mHandler.postDelayed(r, 1000);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Runnable r = new Runnable() {
        @Override
        public void run() {
            try {
                Workout w = (workoutsDao.queryForEq("id",mWorkoutId)).get(0);
                ArrayList<GpsPoint> p = new ArrayList<>();

                p.addAll(w.getGpsPoints());

                if(mTimer == 15){
                    mTimer = 0L;

                    draw(p);
                }else{
                    LatLng currentLocation = new LatLng((p.get(p.size()-1)).getLatitude(),
                            (p.get(p.size()-1)).getLongitude());

                    mGoogleMap.addMarker(new MarkerOptions().position(currentLocation));
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 14));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mTimer++;
            mHandler.postDelayed(this, 1000);
        }
    };

    private Dao<Workout, Long> workoutsDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private Dao<User, Long> userDao;

    private void getDaos(){
        workoutsDao = null;
        gpsPointsDao = null;
        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try{
            workoutsDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
            userDao = databaseHelper.userDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }


    private GoogleMap mGoogleMap;

    private void draw(ArrayList<GpsPoint> points){

        for(int i = 0; i < points.size() - 1; i++){
            LatLng start = new LatLng(points.get(i).getLatitude(), points.get(i).getLongitude());
            LatLng end = new LatLng(points.get(i+1).getLatitude(), points.get(i+1).getLongitude());

            mGoogleMap.addPolyline(new PolylineOptions()
                    .add(start, end)
                    .width(5)
                    .color(Color.BLUE));
        }

        LatLng currentLocation = new LatLng((points.get(points.size()-1)).getLatitude(),
                                            (points.get(points.size()-1)).getLongitude());

        mGoogleMap.addMarker(new MarkerOptions().position(currentLocation));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 14));
    }

    public void setSupportMapFragment(){
        SupportMapFragment smf = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_activeworkoutmap_map);
        smf.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
            }
        });
    }

    @OnClick(R.id.button_activeworkoutmap_back)
    public void back(){
        this.finish();
    }
}
