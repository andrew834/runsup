package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.ActiveWorkoutMapActivity;
import si.uni_lj.fri.pbd2019.runsup.Commands;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

public class StopwatchFragment extends Fragment {

    private Long workoutId = 0l;
    private TextView mDuration;
    private TextView mDistance;
    private TextView mCalories;
    private TextView mPace;
    private Button mMapButton;

    private Intent mTrackerService;

    private ArrayList<List<Location>> mListListLocations;
    private ArrayList<Location> mCurrentPoints;

    public int CURRENT_STATE = 0;
    public static final int START_STATE = 0;
    public static final int RUNNING_STATE = 1;
    public static final int END_STATE = 2;

    public double calories = 0.0;
    public long duration = 0;
    public double pace = 0.0;
    public double distance = 0.0;

    public Workout mLastRunningWorkout;

    private static int SPORT_ACTIVITY_TYPE = 0;
    public BroadcastReceiver mBroadcastReceiver;

    public StopwatchFragment() { }

    public static StopwatchFragment newInstance() {
        StopwatchFragment fragment = new StopwatchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("KAJ==?","oncreate");

        if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("gps_checking", true)){
            // lahko
            if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        100);
            }
        }else{
            if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){            Toast.makeText(getActivity(),"For a better experience we advise you to enable your GPS." ,Toast.LENGTH_SHORT);

                    Toast.makeText(getActivity(),"For a better experience we advise you to enable your GPS." ,Toast.LENGTH_LONG).show();
            }
        }

        mListListLocations = new ArrayList<>();
        mCurrentPoints     = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_stopwatch, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDuration = getView().findViewById(R.id.textview_stopwatch_duration);
        mDistance = getView().findViewById(R.id.textview_stopwatch_distance);
        mCalories = getView().findViewById(R.id.textview_stopwatch_calories);
        mPace     = getView().findViewById(R.id.textview_stopwatch_pace);
        mMapButton= getView().findViewById(R.id.button_stopwatch_activeworkout);

        mMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActiveWorkoutMapActivity.class);
                intent.putExtra("workoutId", workoutId);
                startActivity(intent);
            }
        });

        final Button startWorkout = getView().findViewById(R.id.button_stopwatch_start);
        startWorkout.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Button start = getView().findViewById(R.id.button_stopwatch_start);
                Button end = getView().findViewById(R.id.button_stopwatch_endworkout);

                switch(CURRENT_STATE){
                    case START_STATE:
                        // Zamenjaj napis na gumbu START
                        start.setText(R.string.stopwatch_stop);
                        CURRENT_STATE = RUNNING_STATE;

                        // Registriraj receiver
                        getActivity().registerReceiver(mBroadcastReceiver, new IntentFilter(Commands.TICK));

                        mTrackerService = new Intent(getActivity(), TrackerService.class)
                                .setAction(Commands.COMMAND_START)
                                .putExtra("sports_activity", SPORT_ACTIVITY_TYPE);

                        getActivity().startService(mTrackerService);
                        break;
                    case RUNNING_STATE:
                        // Zamenjaj napis na gumbu STOP -> CONTINUE in prikazi END WORKOUT
                        start.setText(R.string.stopwatch_continue);
                        end.setVisibility(View.VISIBLE);
                        CURRENT_STATE = END_STATE;

                        mTrackerService = new Intent(getActivity(), TrackerService.class)
                                .setAction(Commands.COMMAND_PAUSE);

                        getActivity().startService(mTrackerService);
                        break;
                    case END_STATE:
                        // Zamenjaj napis na gumbu CONTINUE -> STOP
                        start.setText(R.string.stopwatch_stop);
                        end.setVisibility(View.GONE);
                        CURRENT_STATE = RUNNING_STATE;

                        mTrackerService = new Intent(getActivity(), TrackerService.class)
                                .setAction(Commands.COMMAND_CONTINUE);

                        getActivity().startService(mTrackerService);
                        break;
                }
            }
        });

        final Button endWorkout = getView().findViewById(R.id.button_stopwatch_endworkout);
        endWorkout.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                // Sprozi pogovorno okno
                AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                b.setTitle("End Actual Workout?");

                final TextView tw = new TextView(getActivity());
                tw.setText("Do you really want to end workout and reset counters?");
                tw.setPadding(40,0,0,0);
                b.setView(tw);
                b.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CURRENT_STATE = START_STATE;

                        Button start = getActivity().findViewById(R.id.button_stopwatch_start);
                        Button end = getActivity().findViewById(R.id.button_stopwatch_endworkout);

                        Intent intent = new Intent(getActivity(), WorkoutDetailActivity.class);
                        intent.putExtra("calories", calories);
                        intent.putExtra("duration", duration);
                        intent.putExtra("pace", pace);
                        intent.putExtra("sportActivity", SPORT_ACTIVITY_TYPE);
                        intent.putExtra("distance", distance);
                        intent.putExtra("finalPositionList", mListListLocations);
                        intent.putExtra("workoutId", workoutId);

                        start.setText(R.string.stopwatch_start);
                        end.setVisibility(View.GONE);

                        mTrackerService = new Intent(getActivity(), TrackerService.class)
                                .setAction(Commands.COMMAND_STOP);

                        getActivity().startService(mTrackerService);
                        startActivity(intent);
                        // posodobi layout na default

                    }
                });

                b.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                b.show();
            }
        });

        final Button selectWorkout = getView().findViewById(R.id.button_stopwatch_selectsport);
        selectWorkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                mBuilder.setTitle("Choose an activity");

                final ArrayAdapter<String> mAA = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_selectable_list_item);
                mAA.addAll(new String[]{"Running", "Walking", "Cycling"});
                mBuilder.setAdapter(mAA, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectWorkout.setText(mAA.getItem(which));
                        SPORT_ACTIVITY_TYPE = which;

                        mTrackerService = new Intent(getActivity(), TrackerService.class)
                                .setAction(Commands.UPDATE_SPORT_ACTIVITY)
                                .putExtra("sports_activity",which);

                        getActivity().startService(mTrackerService);
                    }
                });

                mBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                mBuilder.show();
            }
        });

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent i) {
                workoutId = i.getLongExtra("workoutId",1l);
                duration = i.getLongExtra("duration", 0);
                calories = i.getDoubleExtra("calories", 0.0);
                pace = i.getDoubleExtra("pace", 0.0);
                distance = i.getDoubleExtra("distance", 0.0);

                mDuration.setText(MainHelper.formatDuration(i.getLongExtra("duration", 0)));

                if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("unit", "0").equals("0")){
                    // default enota
                    mDistance.setText(MainHelper.formatDistance(i.getDoubleExtra("distance", 0.0)));
                    mPace.setText(MainHelper.formatPace(i.getDoubleExtra("pace", 0.0)));
                }else{
                    // miles
                    mDistance.setText(MainHelper.formatTwoDecimals(MainHelper.kmToMi(i.getDoubleExtra("distance", 0.0)/1000)));
                    mPace.setText(MainHelper.formatPace(MainHelper.minpkmToMinpmi(i.getDoubleExtra("pace", 0.0))));

                }

                mCalories.setText(MainHelper.formatCalories(i.getDoubleExtra("calories", 0.0)));

                int currentState = i.getIntExtra("state", 0);
                Log.e(Commands.ERROR, "Current state in receiver: " + currentState);

                Object l = i.getSerializableExtra("location");

                if(l != null){
                    mCurrentPoints.add((Location)l);
                }
                //Log.e(Commands.ERROR, mCurrentPoints.toString());

                if(currentState == Commands.CURRENT_STATE.get("STATE_PAUSED")){
                    mListListLocations.add(mCurrentPoints);
                    mCurrentPoints = new ArrayList<>();
                }
            }
        };

        getDaos();

        QueryBuilder<Workout, Long> qb = workoutsDao.queryBuilder();

        qb.orderBy("id",false); // false for descending order
        qb.limit(1l);

        try {
            List<Workout> rawResults = qb.query();
            if(rawResults.size() == 0){
                // nothing
            }else{
                Workout w = rawResults.get(0);
                Log.d("KAJ!", ""+w.getStatus());
                if(w.getStatus() == 0 || w.getStatus() == 2){
                    Button start = getView().findViewById(R.id.button_stopwatch_start);
                    Button end = getView().findViewById(R.id.button_stopwatch_endworkout);
                    mLastRunningWorkout = w;
                    // POSODOBI UI IN PODAJ VREDNOSTI V TRACKER SERVICE
                    mDuration.setText(MainHelper.formatDuration(w.getDuration()));
                    mCalories.setText(""+MainHelper.formatCalories(w.getTotalCalories()));

                    selectWorkout.setText(Commands.ACTIVITY_TYPE_STRING.get(w.getSportActivity()));

                    if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("unit", "0").equals("0")){
                        // default enota
                        mDistance.setText(MainHelper.formatDistance(w.getDistance()));
                        mPace.setText(MainHelper.formatPace(w.getPaceAvg()));
                    }else{
                        // miles
                        mDistance.setText(MainHelper.formatTwoDecimals(MainHelper.kmToMi(w.getDistance())));
                        mPace.setText(MainHelper.formatPace(MainHelper.minpkmToMinpmi(w.getPaceAvg())));
                    }

                    /*ForeignCollection<GpsPoint> allPoints = w.getGpsPoints();

                    int sessionNumberTemp = 1;
                    ArrayList<Location> tempLoc = new ArrayList<>();

                    for(GpsPoint iterate : allPoints){
                        if(iterate.getSessionNumber() == sessionNumberTemp){
                            Location l = new Location("");
                            l.setLatitude();
                            tempLoc.add(new Location(""));
                        }
                    }*/

                    start.setText(R.string.stopwatch_continue);
                    end.setVisibility(View.VISIBLE);
                    CURRENT_STATE = END_STATE;
                    duration = w.getDuration();
                    pace = w.getPaceAvg();
                    distance = w.getDistance();
                    calories = w.getTotalCalories();
                    SPORT_ACTIVITY_TYPE = w.getSportActivity();
                    workoutId = w.getId();
                    Log.d("HAD","GOT FROM BASE: "+workoutId);

                    mTrackerService = new Intent(getActivity(), TrackerService.class).setAction(Commands.SYNC_ACTIVITY).putExtra("workoutId", w.getId());
                    getActivity().startService(mTrackerService);
                    getActivity().registerReceiver(mBroadcastReceiver, new IntentFilter(Commands.TICK));

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Dao<Workout, Long> workoutsDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private Dao<User, Long> userDao;

    private void getDaos(){
        workoutsDao = null;
        gpsPointsDao = null;
        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        try{
            workoutsDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
            userDao = databaseHelper.userDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }


    @Override
    public void onPause(){
        super.onPause();
        try{
            if(CURRENT_STATE == END_STATE){
                getActivity().startService(mTrackerService.setAction(Commands.COMMAND_PAUSE));
                getActivity().unregisterReceiver(mBroadcastReceiver);
            }
        }catch(Exception e){
            // do nothing
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("unit", "0").equals("0")){
            // default enota
            mDistance.setText(MainHelper.formatDistance(distance));
            mPace.setText(MainHelper.formatPace(pace));

            String paceUnit = getResources().getString(R.string.all_min) + "/" + getResources().getString(R.string.all_labeldistanceunitkilometers);

            ((TextView)getView().findViewById(R.id.textview_stopwatch_distanceunit)).setText(getResources().getString(R.string.all_labeldistanceunitkilometers));
            ((TextView)getView().findViewById(R.id.textview_stopwatch_unitpace)).setText(paceUnit);

        }else{
            // miles
            double tempKAJ = MainHelper.kmToMi(distance/1000);
            String tempKAJ2 = MainHelper.formatTwoDecimals(tempKAJ);
            mDistance.setText(tempKAJ2);
            mPace.setText(MainHelper.formatPace(MainHelper.minpkmToMinpmi(pace)));

            String paceUnit = getResources().getString(R.string.all_min) + "/" + getResources().getString(R.string.all_labeldistanceunitmiles);

            ((TextView)getView().findViewById(R.id.textview_stopwatch_distanceunit)).setText(getResources().getString(R.string.all_labeldistanceunitmiles));
            ((TextView)getView().findViewById(R.id.textview_stopwatch_unitpace)).setText(paceUnit);
        }

            try{
            if(CURRENT_STATE == END_STATE){
                mTrackerService = mTrackerService.setAction(Commands.COMMAND_PAUSE);
                getActivity().startService(mTrackerService);
                getActivity().registerReceiver(mBroadcastReceiver, new IntentFilter(Commands.TICK));
            }
        }catch(Exception e){
            // do nothing
        }
    }

    private void showToastText(String text){
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            //mTrackerService = new Intent(getActivity(), TrackerService.class).setAction(Commands.COMMAND_PAUSE);
            getActivity().stopService(mTrackerService);
            //getActivity().startService(mTrackerService);
            getActivity().unregisterReceiver(mBroadcastReceiver);
        }catch(Exception e){
            // do nothing
        }
    }
}
