package si.uni_lj.fri.pbd2019.runsup;

import android.content.DialogInterface;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class LoginActivity extends AppCompatActivity {

    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mFirebaseAuth;
    private static final int RC_SIGN_IN = 9001;

    @BindView(R.id.btn_login)
    public SignInButton mLoginButton;

    @BindView(R.id.weight_selector)
    public TextView mWeight;

    @BindView(R.id.age_selector)
    public TextView mAge;

    @BindView(R.id.height_selector)
    public TextView mHeight;

    @Override
    protected void onStart() {
        super.onStart();
        updateGUI();
    }

    private void updateGUI(){
        FirebaseUser user = mFirebaseAuth.getCurrentUser();

        if(user != null){
            // logged in
            switchText("Sign out");
        }else{
            // logged out
            switchText("Sign in");
        }
    }

    private void switchText(String text){
        for (int i = 0; i < mLoginButton.getChildCount(); i++) {
            View v = mLoginButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setText(text);
                return;
            }
        }
    }

    private Dao<UserProfile, Long> userProfileDao;
    private Dao<User, Long> userDao;

    private void getDaos(){

        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try{
            userProfileDao = databaseHelper.userProfileDao();
            userDao = databaseHelper.userDao();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupActionBar();

        ButterKnife.bind(this);

        getDaos();
        QueryBuilder getUser = userDao.queryBuilder();
        List<User> u = null;

        try {
            u = (List<User>)(getUser.query());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        User userData = u.get(0);
        try {
            List<UserProfile> userProfiles = userData.getUserProfiles().getDao().queryBuilder().query();
            if(userProfiles.size() > 0){
                UserProfile up = userProfiles.get(userProfiles.size() - 1);
                mWeight.setText(up.getWeight() + " kg");
                mHeight.setText(up.getHeight() + " cm");
                mAge.setText(up.getAge() + " years");
            }else{
                UserProfile newProfile = new UserProfile();
                newProfile.setAge(25);
                newProfile.setHeight(180);
                newProfile.setWeight(60);
                newProfile.setUser(userData);
                userProfileDao.create(newProfile);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();



        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mFirebaseAuth = FirebaseAuth.getInstance();
    }

    @OnClick(R.id.btn_login)
    public void login(){

        FirebaseUser user = mFirebaseAuth.getCurrentUser();

        if(user != null){
            signout();
        }else{
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(RC_SIGN_IN == requestCode) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                AuthCredential authCredential = GoogleAuthProvider.getCredential(account.getIdToken(),null);
                final String oauthToken = account.getIdToken();
                mFirebaseAuth.signInWithCredential(authCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            getDataAndUpdateUI(mFirebaseAuth.getCurrentUser(), oauthToken);
                        }else{
                            Log.e("Firebase", "Login failed :(.");
                        }
                    }
                });
            } catch (ApiException e) {
                Log.d("Firebase", "Firebase error: " + e.toString());
            }
        }
    }

    protected void getDataAndUpdateUI(FirebaseUser user, String oauthToken){
        String fullname = user.getDisplayName();
        String photoUrl = user.getPhotoUrl().toString();

        userDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);

        try{
            userDao = databaseHelper.userDao();
            QueryBuilder getUser = userDao.queryBuilder();
            List<User> u = (List<User>)(getUser.query());
            User userData = u.get(0);

            userData.setAccId(user.getUid());
            userData.setAuthToken(oauthToken);

            userDao.update(userData);

        }catch(SQLException e){
            e.printStackTrace();
        }

        PreferenceManager.getDefaultSharedPreferences(this).edit().putLong("userId", 1).commit();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("userSignedIn", true).commit();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("userId", user.getUid()).commit();

        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("fullname", fullname).commit();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("picture", photoUrl).commit();

        updateGUI();
    }

    public void signout(){
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(LoginActivity.this, "User logged out.", Toast.LENGTH_SHORT).show();
                mFirebaseAuth.signOut();

                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putBoolean("userSignedIn", false).commit();
                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("userId", "").commit();

                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().remove("fullname").commit();
                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().remove("picture").commit();

                updateGUI();
            }
        });
    }

    @OnClick(R.id.weight_label)
    public void weight(){
        createDialog("Weight", InputType.TYPE_CLASS_NUMBER);
    }

    @OnClick(R.id.age_label)
    public void age(){
        createDialog("Age", InputType.TYPE_CLASS_NUMBER);
    }

    @OnClick(R.id.height_label)
    public void height(){
        createDialog("Height", InputType.TYPE_CLASS_NUMBER);
    }

    public void createDialog(String title, int type){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle(title);

        final String whichTitle = title;
        final EditText editText = new EditText(this);
        editText.setInputType(type);

        mBuilder.setView(editText);

        mBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String text = editText.getText().toString();

                if(text.length() == 0){
                    Toast.makeText(LoginActivity.this, "We require valid text.", Toast.LENGTH_SHORT).show();
                    return;
                }

                List<User> u = null;
                QueryBuilder getUser = userDao.queryBuilder();

                try {
                    u = (List<User>)(getUser.query());
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                User userData = u.get(0);
                UserProfile up = null;
                try {
                    List<UserProfile> userProfiles = userData.getUserProfiles().getDao().queryBuilder().query();
                    if(userProfiles.size() > 0){
                        up = userProfiles.get(userProfiles.size() - 1);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if(whichTitle.equals("Weight")){
                    mWeight.setText(text + " kg");
                    up.setWeight(Double.parseDouble(text));
                }else if(whichTitle.equals("Age")){
                    mAge.setText(text + " years");
                    up.setAge(Integer.parseInt(text));
                }else if(whichTitle.equals("Height")){
                    mHeight.setText(text + " cm");
                    up.setHeight(Double.parseDouble(text));
                }

                try {
                    if(up != null){
                        userProfileDao.update(up);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        mBuilder.show();
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


}
