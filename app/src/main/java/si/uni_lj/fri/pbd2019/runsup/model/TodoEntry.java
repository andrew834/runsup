package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "TodoEntry")
public class TodoEntry {
    public TodoEntry(String title, String description, Date whenDate, User user) {
        this.title = title;
        this.description = description;
        this.whenDate = whenDate;
        this.user = user;
    }

    public TodoEntry(){

    }

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(canBeNull = false, columnName = "title")
    private String title;

    @DatabaseField(canBeNull = false, columnName = "description")
    private String description;

    @DatabaseField(canBeNull = false, columnName = "whenDate")
    private Date whenDate;

    @DatabaseField(foreign = true, columnName = "user")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getWhenDate() {
        return whenDate;
    }

    public void setWhenDate(Date whenDate) {
        this.whenDate = whenDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
